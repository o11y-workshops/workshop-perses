Try the [workshop online](https://o11y-workshops.gitlab.io/workshop-perses):

[![Cover Slide](cover.png)](https://o11y-workshops.gitlab.io/workshop-perses)

### Abstract
Are you ready for getting started with cloud native observability with telemetry pipelines? This workshop will guide 
you through the open source project Fluent Bit, what it is, a basic installation, and setting up a first telemetry 
pipeline project. Learn how to manage your cloud native data from source to destination using the telemetry pipeline 
phases covering collection, aggregation, transformation, and forwarding from any source to any destination. Buckle up 
for a fun ride as you learn by doing, exploring how telemetry pipelines work, how to set up your first pipeline, and 
exploring several common use cases that Fluent Bit helps solve.

Released versions
-----------------
See the tagged releases for the following versions of the product:

 - v0.50 - Workshop based on Perses v0.50.0.

 - v0.47 - Workshop based on Perses v0.47.1.

 - v0.42 - Workshop based on Perses v0.42.0.

 - v0.21 - Workshop based on Perses v0.21.0.